var express = require('express');
var router = express.Router();
var basicAuth = require('express-basic-auth');
var bcrypt = require('bcrypt');

module.exports = router;

router.use(basicAuth({
    authorizer: myAsyncAuthorizer,
    authorizeAsync: true,
    unauthorizedResponse: getUnathorizedResponse
}));

function myAsyncAuthorizer(username, password, callback){
    var storedUsername = process.env.BASIC_AUTH_USER;
    var storedPwd = process.env.BASIC_AUTH_PASS;
    bcrypt.compare(username, storedUsername, function(error, response){
        if(response){
            bcrypt.compare(password, storedPwd, function (error, response) {
                if(response){
                    callback(null, true);
                }else{
                    console.log("Error with password.")
                    callback(null, false)
                }
            })
        }else{
            console.log("Error with username: " + username);
            callback(null, false);
        }
    });
}

//Unathorized response, or creating a new user if the request hit the /signupnew.
function getUnathorizedResponse(req){
        return req.auth ?
            ('Credential ' + req.auth.user + '\'s password rejected') :
            'No credentials provided.'
}
