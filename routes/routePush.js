var express = require('express');
var router = express.Router();
//Parser required for Express to consume JSON
var jsonParser = require('body-parser').json();
//Importing functions from other files
var postToGigya = require('./gigyaRequest').postToGigya;

/* Ping method tied to GET, to ensure application is up */
router.get('/', (req, res) => {
  res.send('Ping!');
});

//Post method that will send the data to Gigya and send the response back to ONE as a status code, 200 if good. 500 if
//something went wrong.
router.post('/', jsonParser, (req, res) => {
    //The function above returns the new formatted JSON and the gigyaUID
    postToGigya(null, req.body, function(success){
      //If success is true, everything went correctly
      if(success){
        res.sendStatus(success.statusCode);
      } else{
        res.sendStatus(success.statusCode);
      }
    })

});

router.put('/', jsonParser, (req, res) => {
    postToGigya(req.query.UID, req.body, function(success){
        //If success is true, everything went correctly
        if(success){
            res.sendStatus(success.statusCode);
        } else{
            res.sendStatus(success.statusCode);
        }
    })
})

module.exports = router;

