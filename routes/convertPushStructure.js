module.exports = {
    // convert ONE Push structure into a standard JSON object
    convertPushStructure: function(pushStructure, gigyaUid, callback){
            var structure = {};
            var temporaryObj = {};
            //Iterate over the elements inside the ONE Push structure
            for (var idxElement = 0; idxElement < pushStructure.root.elements.length; idxElement++) {
                //Setting the structure as the current top attribute available
                var attribute = pushStructure.root.elements[idxElement];
                //Iterate over the elements in the top attribute
                if (attribute.elements) {
                    //If its the first time iterating over the element, give this attribute a value
                    if(idxElement === 0){
                        structure[attribute.name] = attribute.data;
                    }
                    //Lets go deeper into the elements within the attribute
                    temporaryObj = module.exports.convertPushStructureElements(attribute.elements);

                    //Lets make sure that the top level of the JSON we are sending is named thunderhead
                    if(structure.thunderhead === undefined){
                        structure.thunderhead = temporaryObj;
                    }
                }else {
                    //The top attribute does not have any elements, so lets just send it back
                    structure[attribute.name] = attribute.data;
                }
            }
            //If the structure has a UID element, lets grab it and send it out
            if(structure.thunderhead.UID){
                var gigyaUid = structure.thunderhead.UID;
                delete structure.thunderhead.UID;
                callback(structure, gigyaUid);
            }else{
                callback(structure, gigyaUid);
            }
        },
        //function to recursively keep checking if the attributes contain an element within them
    convertPushStructureElements: function(pushStructure){
        //Creating an empty object to store our values while we iterate through the bigger one
        var tempSmallerObjs = {};
        //iterate through the structure
        for (var indxElement = 0; indxElement < pushStructure.length; indxElement++) {
            //Setting the structure as the current top attribute available
            var attribute = pushStructure[indxElement];
            //Iterate over the elements in the current attribute if the current attribute has an elements in it,
            //then we will send it back through this same function
            if (attribute.elements) {
                //Send this current structure to this same function until we have iterated through all of the
                //attributes and no more elements are left, then return that value to the temporary object
                tempSmallerObjs[attribute.name] = module.exports.convertPushStructureElements(attribute.elements);
            }
            else {
                //we are done with this attribute, lets make sure the temporary object has the values we want
                tempSmallerObjs[attribute.name] = attribute.data;
            }
        }
        //send the current attribute with the structure we want back
        return tempSmallerObjs;

    },

    gigyaCavsStructure: function(objectToChange){
        var data = {};
        var thunderhead = {};
        var nameOfVariable, nameOfSmallerVariable;

        for(var key in objectToChange){
            nameOfVariable = "thunderhead." + key;
            if(objectToChange[key].toString() === "[object Object]"){
                thunderhead = module.exports.gigyaRecursiveStructure(objectToChange[key], nameOfVariable, thunderhead);
            }else{
                nameOfSmallerVariable = nameOfVariable;
                thunderhead[nameOfSmallerVariable] = objectToChange[key];
            }


        }
        data = thunderhead;
        return data;

    },

    gigyaRecursiveStructure: function (structureToChange, nameOfVariable, thunderhead) {
        var nameOfSmallerVariable;

        for(var anotherKey in structureToChange){
            if(structureToChange[anotherKey].toString() === "[object Object]"){
                nameOfSmallerVariable = nameOfVariable + "." +anotherKey;
                thunderhead[nameOfSmallerVariable] = module.exports.gigyaRecursiveStructure(
                    structureToChange[anotherKey], nameOfSmallerVariable, thunderhead)
            }else{
                nameOfSmallerVariable = nameOfVariable + "." +anotherKey;
                thunderhead[nameOfSmallerVariable] = structureToChange[anotherKey];
            }
        }
        return thunderhead;
    }

};