var express = require('express');
var router = express.Router();
var auth = require('./auth');

//Setting up Basic auth and make sure we use it for all of our endpoints
router.use(auth);

//Point to where we have our endpoint configuration
router.use('/account', require('./routePush'));

module.exports = router;
