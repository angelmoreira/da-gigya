var request = require('request');
//Setting up the Gigya client library
var Gigya = require('gigya').Gigya;
//Grabbing the values from Azure Application Settings
var gigyaApiKey = (process.env.GIGYA_API_KEY);
var gigyaDataCenter = (process.env.GIGYA_DATA_CENTER);
var gigyaSecretKey = (process.env.GIGYA_SECRET_KEY);
var userGigya = (process.env.GIGYA_USER);
//importing functions
var convertJson = require('./convertPushStructure').convertPushStructure;
var gigyaFormat = require('./convertPushStructure').gigyaCavsStructure;

//Setting up the gigya client with values provided
const gigya = new Gigya(gigyaApiKey, gigyaDataCenter, userGigya, gigyaSecretKey);

module.exports = {
    //Function to handle send to Gigya, gigyaUid grabbed from ONE
    //dataToSend has been reformatted in a traditional JSON setting
     postToGigya: function(gigyaUid, pushStructure, callback){
         var dataToSend;
         convertJson(pushStructure, gigyaUid, function(newJsonObject, gigyaUid){
           if(!gigyaUid){
               console.log("Error grabbing Gigya UID, please verify that it resides in the top " +
                   "level of the structure.")
           }else {
               dataToSend = gigyaFormat(newJsonObject.thunderhead)
               module.exports.requestGigya(gigyaUid, dataToSend, callback)
           }
         })
    },

    requestGigya: function(gigyaUid, dataToSend, callback){
            //Pushing data to Gigya
            var gigyaSetAccount = gigya.accounts.setAccountInfo({
                UID: gigyaUid,
                data: dataToSend
            });
            //Promise
            gigyaSetAccount.then((res) => {
                callback(res);
            });
            //Catch in case of error
            gigyaSetAccount.catch((error) => {
                console.log("There was an error: " + error);
                callback(error);
            });
    }
}

