var express = require('express');
var app = express();

//making sure to use the routes we set up
app.use(require('./routes'));

//setting up listener that will direct the incoming requests
var server = app.listen(process.env.PORT || 3000, function() {
    console.log('App up on: %s...', server.address().port);
});